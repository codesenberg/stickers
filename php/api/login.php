<?
require ('config.php');
const DEFAULT_PATH = '/users';

//check for missing params
errorHandler(['uid', 'password']);

$fbResponse = $firebase->get(DEFAULT_PATH);

$results = (object) [];
$postCompiled = (object) [];
$loginStatus = false;
$userFound = false;

$fbResponse = json_decode($fbResponse);
$users = $fbResponse;

foreach ($users as $userId => $user) {
    if (strtolower($postData->uid) === strtolower($user->profile->username)) {
        $userFound = true;
        if( crypt($postData->password,PASSWORD_SALT) === PASSWORD_SALT . $user->profile->password) {
            $loginStatus = true;
            break;
        }
    }
}

if ($loginStatus) {
    $results->status = 'success';
    $results->error = '';
} else if ($userFound && !$loginStatus) {
    $results->status = 'error';
    $results->error = 'Incorrect user or password';
} else {
    $results->status = 'error';
    $results->error = 'User not registered';
}

print_r(json_encode($results));
