<?
require ('config.php');
const DEFAULT_PATH = '/users';

//check for missing params
errorHandler(['uid']);

$fbResponse = $firebase->get(DEFAULT_PATH);

$results = (object) [];
$postCompiled = (object) [];

$fbResponse = json_decode($fbResponse);
$users = $fbResponse;

$likedPosts = $users->{$postData->uid}->likedPosts;

unset($users->{$postData->uid});

foreach ($users as $userId =>  $user) {
    foreach ($user->posts as $postId => $post) {
        $post->{'ownerId'} = $userId;
        $post->{'userIcon'} = API_SERVER_URL . "users/{$postData->uid}/profile-image/main.png";
        foreach ($post as $key => $value) {
            //prepare image info
            if ($key === 'image') {
                if ($value) {
                    $post->$key = API_SERVER_URL . "users/{$userId}/posted-images/{$postId}.png";
                //if no image is provide remove image data
                } else {
                    unset($post->$key);
                    unset($post->imageLikedTotal);
                }

            }

            //if no text is provided remove text data
            if ($key === 'text') {
                if ($value === '') {
                    unset($post->$key);
                    unset($post->textLikedTotal);
                }

            }
        }

        //store compiled post
        $postCompiled->{$postId} = $post;
    }
}

$results->likedPosts = $likedPosts;
$results->posts = $postCompiled;

print_r(json_encode($results));
