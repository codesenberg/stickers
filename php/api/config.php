<?php

error_reporting(E_ALL);
ini_set('display_errors', 1);
require('../firebase-php/src/firebaseInterface.php');
require('../firebase-php/src/firebaseLib.php');
const DB_URL = 'https://cb-dev-stickers.firebaseio.com/';
const API_SERVER_URL = 'http://localhost:8888/php/';
const PASSWORD_SALT = '$1$a5uKTmNV$'; //CRYPT_MD5
                       //OstKxPz3Dm7XVURgF/wzN0
$firebase = new \Firebase\FirebaseLib(DB_URL);
header('Content-type: application/json');

//handles GET data when testing in browswer otherwise handles json post data from application
if (sizeof($_GET) > 0) {
    $postData = (object) $_GET;
} else {
    $postData = json_decode(file_get_contents('php://input'));
}

//$dataArray expecting keys contained in postData
//NOTE, i need be I can make this more complex by passing customized conditionals
function errorHandler($dataArray)  {
    global $postData;
    $missingArgs = [];

    for ($i = 0; $i < sizeof($dataArray); $i++) {
        if (!isset($postData->{$dataArray[$i]})) {
            array_push($missingArgs, $dataArray[$i]);
        }
    }
    if (sizeof($missingArgs) > 0) {
        header("HTTP/1.1 500 Internal Server Error");
        echo 'The following arguments are missing: ', implode(', ', $missingArgs) ;
        exit;
    }
}
