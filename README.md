# Stickers Mobile Social App Prototype

In the middle of prototyping a social app in React Native with Redux which currently contains proof of concepts for the following functionality:

- Login
- Displaying social data screens api calls
- Taking photos
- Saving photos to remote server

Uses php server to communicate to firebase db for all api calls. Considerations to remove php layer and have app directly access firebase through react
native.



## Setup

```
$ git clone https://codesenberg@bitbucket.org/codesenberg/stickers.git
$ npm install
```

## Running

Ensure that you have Xcode installed on your machine (https://developer.apple.com/support/xcode/)

```
$ npm run php-server
$ npm run open

```
When Xcode is open, press the play button to run the simulator. You may use the following login:
uname: kakashi
pword: 1234qwer

# License

MIT
