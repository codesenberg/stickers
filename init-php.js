var phpServer = require('node-php-server');
 
// Create a PHP Server 
phpServer.createServer({
    port: 8888,
    hostname: '127.0.0.1',
    base: '.',
    keepalive: false,
    open: false,
    bin: 'php',
    router: __dirname + '/php/server.php'
});

console.log('PHP server running...');
console.log('hostname: 127.0.0.1');
console.log('port: 8888');