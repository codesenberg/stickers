import ExploreConstants from '../constants/ExploreConstants';
import api from '../../apiHandler/api';

const loadPosts = () => {
    return {
        type: ExploreConstants.LOAD_POSTS,
        exploresLoaded: false
    }
}

const explorePosts = (posts, likedPosts) => {
    return {
        type: ExploreConstants.EXPLORE_POSTS,
        exploresLoaded: true,
        posts,
        likedPosts
    }
}

const likePost = (postId, contentType, status) => {
    return {
        type: ExploreConstants.EXPLORE_LIKE_POST,
        postId,
        contentType,
        status
    }
}

//@param params object defines:
//string uid current user id
//TODO add long/lat as params
export function fetchExplores(params) {
    return dispatch => {
        dispatch(loadPosts());
        return api.post('http://localhost:8888/php/api/explore-posts.php', params)
          .then(response => response.json())
          .then(json => dispatch(explorePosts(json.posts, json.likedPosts)))
          .catch((error) => {
              return Promise.resolve(error);
          });
    }
}

//@param params object defines:
//string uid current user id
//string postId post being likedPosts
//string type 'image' or 'text'
//boolean status true for like, false for unlike
export function fetchLikePost(params) {
    //console.log(params,'this is params');
    return dispatch => {
        //immediately update UI and revert if error is returned
        dispatch(likePost(params.postId, params.type, params.status));
        return api.post('http://localhost:8888/php/api/like-post.php', params)
          .catch((error) => {
              dispatch(likePost(params.postId, params.type, !params.status));
              return Promise.resolve(error);
          });
    }
}
