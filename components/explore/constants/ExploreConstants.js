
var keyMirror = require('keymirror');

module.exports = keyMirror({
    EXPLORE_POSTS: null,
    EXPLORE_LIKE_POST: null,
    LOAD_POSTS: null
});
