import ExploreConstants from '../constants/ExploreConstants';

const exploreStore = (state = {}, action) => {
    switch (action.type) {
    case ExploreConstants.LOAD_POSTS:
        return {
            ...state,
            exploresLoaded: action.exploresLoaded
        };
    case ExploreConstants.EXPLORE_POSTS:
        if (state.hasOwnProperty('posts')){
            delete state.likedPosts.user1_post1Unique;
            action.likedPosts = false;
        }
        
        return {
            ...state,
            posts: action.posts,
            exploresLoaded: action.exploresLoaded,
            likedPosts: action.likedPosts
        };
    case ExploreConstants.EXPLORE_LIKE_POST:
        let likedPosts = Object.assign({}, state.likedPosts);
        if (likedPosts[action.postId] === undefined) {
            likedPosts[action.postId] = {};
        }
        if (action.status) {
            likedPosts[action.postId][action.contentType] = true;
        } else {
            delete likedPosts[action.postId][action.contentType];
        }

        return {
            ...state,
            likedPosts: likedPosts
        };
    default:
        return state
    }
}

export default exploreStore;
