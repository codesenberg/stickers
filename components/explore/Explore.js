'use strict';

import React, {
    StyleSheet,
    Text,
    View,
    Image,
    Component,
    TouchableOpacity
} from 'react-native';
import LikeButtonContainer from './containers/LikeButtonContainer';


class Explore extends Component {
    updateLikeStatus(contentId, status){
        this.props.updateLikeStatus(this.props.id, contentId, status);
    }

    render() {
        let image, message, imageLike, messageLike;
        if (this.props.image) {
            if (this.props.type === 'explore') {
                imageLike = (
                    <View style={styles.likeButton}>
                        <LikeButtonContainer
                            postId={this.props.id}
                            ownerId={this.props.ownerId}
                            contentType={'image'}
                            likeStatus={this.props.imageIsLiked}
                            updateLikeStatus={(contentId, status) => this.updateLikeStatus(contentId, status)}
                        />
                    </View>
                );
            }
            image = (
                <View style={{height:225}}>
                    <Image
                        source={this.props.image}
                        style={styles.exploreImage}
                    />
                    <View style={styles.iconHolder}>
                        <Image
                            source={this.props.userIcon}
                            style={styles.iconImage}
                        />
                    </View>
                    {imageLike}
                </View>
            );
        }

        if (this.props.message) {
            if (this.props.type === 'explore') {
                messageLike = (
                    <View style={styles.likeButton}>
                        <LikeButtonContainer
                            postId={this.props.id}
                            contentType={'text'}
                            likeStatus={this.props.messageIsLiked}
                            updateLikeStatus={(contentId, status) => this.updateLikeStatus(contentId, status)}
                        />
                    </View>
                );
            }
            message = (
                <View style={{position:'relative', height:225, backgroundColor:'red'}}>
                    <View style={styles.iconHolder}>
                        <Image
                            source={this.props.userIcon}
                            style={styles.iconImage}
                        />
                    </View>
                    <View style={{ justifyContent:'space-around', alignItems:'center',paddingTop:80, paddingLeft:20, paddingRight:20}}>
                        <Text style={{ fontSize:18}}>
                            {this.props.message}
                        </Text>
                    </View>
                    {messageLike}
                </View>
            );
        }

        return (
            <View>
                {image}
                {message}
            </View>
        );
    }
}
const styles = StyleSheet.create({
    exploreImage: {
        height:225,
        width:400,
        position:'absolute',
    },
    iconHolder: {
        width: 50,
        height: 50,
        borderRadius: 25,
        shadowOffset: {width:0,height:1},
        shadowColor:'black',
        shadowRadius:2,
        shadowOpacity:.8,
        position:'absolute',
        borderColor:'#000000',
        borderWidth:0.5,
        top:10,
        left:10,
    },
    iconImage: {
        width: 50,
        height: 50,
        borderRadius: 25
    },
    likeCircle:{
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor:'green',
        borderRadius: 25,
        shadowOffset: {width:0,height:1},
        shadowColor:'black',
        shadowRadius:2,
        shadowOpacity:.8,
        borderColor:'#000000',
        borderWidth:0.5,
    },
    likeButton:{
        position:'absolute',
        bottom: 10,
        right: 10,

    }
});

module.exports = Explore;
