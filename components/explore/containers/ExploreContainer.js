import { connect } from 'react-redux';
import Explore from '../Explore';

const mapStateToProps = (state) => {
    return {
        posts: state.exploreStore.posts,
        exploresLoaded: state.exploreStore.exploresLoaded,
        likedPosts: state.exploreStore.likedPosts
    }
}

const ExploreContainer = connect(
  mapStateToProps
)(Explore);

export default ExploreContainer;
