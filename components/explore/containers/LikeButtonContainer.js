import { connect } from 'react-redux';
import LikeButton from '../LikeButton';

const mapStateToProps = (state) => {
    return {
        likedPosts: state.exploreStore.likedPosts
    }
}

const LikeButtonContainer = connect(
  mapStateToProps
)(LikeButton);

export default LikeButtonContainer;
