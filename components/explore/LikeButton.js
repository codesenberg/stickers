'use strict';

import React, {
    StyleSheet,
    View,
    TouchableOpacity,
    Component
} from 'react-native';
import { connect } from 'react-redux';
import * as exploreActions from './actions/exploreAction';
import {bindActionCreators} from 'redux';

class LikeButton extends Component {

    toggleLike() {
        let newLikeStatus = (this.props.likeStatus) ? false:true;
        this.props.fetchLikePost({uid:'user2', postId: this.props.postId, type: this.props.contentType, status: newLikeStatus});
        //this.props.updateLikeStatus(this.props.contentId, newLikeStatus);
        //{uid:'user2'}
    }

    getLikeStyle() {
        let myStyle = [styles.likeCircle];
        if (this.props.likeStatus) {
            myStyle.push(styles.isLiked);
        } else {
            myStyle.push(styles.isNotLiked);
        }

        return myStyle;
    }

    render() {
        return (
            <TouchableOpacity
                onPress={() => this.toggleLike()}
            >
                <View style={this.getLikeStyle()}/>
            </TouchableOpacity>
        );
    }
}
const styles = StyleSheet.create({
    likeCircle:{
        width: 50,
        height: 50,
        borderRadius: 25,
        borderRadius: 25,
        shadowOffset: {width:0,height:1},
        shadowColor:'black',
        shadowRadius:2,
        shadowOpacity:.8,
        borderColor:'#000000',
        borderWidth:0.5
    },
    isLiked:{
        backgroundColor:'orange'
    },
    isNotLiked:{
        backgroundColor:'green'
    }
});

const mapDispatchToProps = (dispatch) => ({
    fetchLikePost: bindActionCreators(exploreActions.fetchLikePost, dispatch)
});

export default connect(
    '',
    mapDispatchToProps
)(LikeButton);
