import ExploreConstants from '../constants/LoginConstants';
import api from '../../apiHandler/api';

const login = (status, error) => {
    console.log('logging functinality worked!!!!')
    return {
        type: ExploreConstants.LOGIN,
        status,
        error
    }
}

//@param params object defines:
//string uname login user name
//string pword login pword
export function fetchLogin(params) {
    return dispatch => {
        console.log('godispatch');
        return api.post('http://localhost:8888/php/api/login.php', params)
          .then(response => response.json())
          .then(json => dispatch(login(json.status, json.error)))
          .catch((error) => {
              return Promise.resolve(error);
          });
    }
}
