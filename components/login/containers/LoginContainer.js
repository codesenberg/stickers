import { connect } from 'react-redux';
import {bindActionCreators} from 'redux';
import Login from '../Login';
import * as loginActions from '../actions/loginAction';

const mapStateToProps = (state) => {
    return {
        status: state.loginStore.status,
        error: state.loginStore.error
    }
}

const mapDispatchToProps = (dispatch) => ({
    fetchLogin: bindActionCreators(loginActions.fetchLogin, dispatch),
});

const LoginContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);


export default LoginContainer;
