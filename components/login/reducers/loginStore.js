import LoginConstants from '../constants/LoginConstants';

const loginStore = (state = {}, action) => {
    switch (action.type) {
    case LoginConstants.LOGIN:
        return {
            ...state,
            status: action.status,
            error: action.error
        };
    default:
        return state
    }
}

export default loginStore;
