'use strict';

import React, {
    StyleSheet,
    Text,
    View,
    TextInput,
    Image,
    Component,
    TouchableOpacity
} from 'react-native';

class Login extends Component {
    constructor() {
        super();
        this.state = {userName:'', password:'', errorMsg:''};
    }

    login() {
        //testing!!!
        //console.log(this.props.fetchLogin,'this is inside Login.js')
        this.props.fetchLogin({uid:'user2'});
        /*let tempUserData = {
            user1: {
                userName:'Omar',
                password:'Plummer'
            },
            user2: {
                userName:'Son',
                password:'Goku'
            }
        };*/
        //let loginStatus = false;
        /*
        {Object.keys(tempUserData).map((key) => {
            if (tempUserData[key].userName === this.state.userName) {
                if (tempUserData[key].password === this.state.password) {
                    loginStatus = true;
                }
            }
        })}*/
        /*
        if (loginStatus) {
            console.log('LOGIN SUCCESSFUL');
        } else {
            this.setState({userName:'', password:'', errorMsg:'Incorrect Login'});
            console.log('LOGIN UNSUCCESSFUL');
        }*/
        this.props.fetchLogin({uid:this.state.userName, password:this.state.password}).then(function(data){
            console.log(data,'what is my data!!!');
        });
    }
    render() {
        return (
            <View>
                <Text style={styles.title}>WELCOME, PLEASE LOGIN</Text>
                <Text>
                    {"Email"}
                </Text>
                <TextInput
                    autoCapitalize={'none'}
                    blurOnSubmit={false}
                    onChangeText={(userName) => this.setState({userName})}
                    style={styles.input}
                    value={this.state.userName}
                />
                <Text>
                    {"Password"}
                </Text>
                <TextInput
                    autoCapitalize={'none'}
                    blurOnSubmit={false}
                    onChangeText={(password) => this.setState({password})}
                    secureTextEntry={true}
                    style={styles.input}
                    value={this.state.password}
                />
                <TouchableOpacity
                    onPress={() => this.login()}
                >
                    <Text style={styles.instruction}>
                        {"Once you enter your username/pword, click screen to login. You may use uname/pword below. Remember to make sure php server is running."}
                    </Text>
                    <Text>
                        {"Status: "}{this.props.status}
                    </Text>
                    <Text>
                        {"Error: "}{this.props.error}
                    </Text>
                    <Text
                        style={styles.forgotPassword}
                    >
                        {"Forgot your password"}
                    </Text>
                    <Text>{"Temp uname: kakashi"}</Text>
                    <Text>{"Temp pword: 1234qwer"}</Text>
                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    instruction: {
        marginBottom: 20
    },
    title: {
        marginTop: 20,
        marginBottom: 20
    },
    input : {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        marginBottom: 20
    },
    forgotPassword: {
        marginTop:50,
        marginBottom:20
    }
});

module.exports = Login;
