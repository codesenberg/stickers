'use strict';

import React, {
    StyleSheet,
    Text,
    View,
    TextInput,
    Image,
    Component,
    TouchableOpacity
} from 'react-native';

class ModalMainMenu extends Component {
    constructor() {
        super();
        let tempCurrentUser = {
            uid: 0,
            fullName: 'Omar Plummer',
            following: 35,
            followers: 50
        }
        
        this.user = tempCurrentUser;
    }

    login() {
        let tempUserData = {
            user1: {
                userName:'Omar',
                password:'Plummer'
            },
            user2: {
                userName:'Son',
                password:'Goku'
            }
        };
        let loginStatus = false;

        {Object.keys(tempUserData).map((key) => {
            if (tempUserData[key].userName === this.state.userName) {
                if (tempUserData[key].password === this.state.password) {
                    loginStatus = true;
                }
            }
        })}

        if (loginStatus) {
            console.log('LOGIN SUCCESSFUL');
        } else {
            this.setState({userName:'', password:'', errorMsg:'Incorrect Login'});
            console.log('LOGIN UNSUCCESSFUL');
        }
    }
    render() {
        return (
            <View>
                <Text>
                    {"Email"}
                </Text>
                <TextInput
                    blurOnSubmit={false}
                    onChangeText={(userName) => this.setState({userName})}
                    style={styles.input}
                    value={this.state.userName}
                />
                <Text>
                    {"Password"}
                </Text>
                <TextInput
                    blurOnSubmit={false}
                    onChangeText={(password) => this.setState({password})}
                    style={styles.input}
                    value={this.state.password}
                />
                <TouchableOpacity
                    onPress={() => this.login()}
                >
                    <Text>
                        {"Start sticking"}
                    </Text>
                    <Text>
                        {this.state.errorMsg}
                    </Text>
                    <Text
                        style={styles.forgotPassword}
                    >
                        {"Forgot your password"}
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
}
const styles = StyleSheet.create({
    input : {
        height: 40,
        borderColor: 'gray',
        borderWidth: 1,
        marginBottom: 20
    },
    forgotPassword: {
        marginTop:50
    }
});

module.exports = Login;
