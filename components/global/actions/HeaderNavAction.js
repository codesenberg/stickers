import HeaderNavConstants from '../constants/HeaderNavConstants';

export const navStateChange = (currentPage) => {
    return {
        type: HeaderNavConstants.HEADERNAV_STATE_CHANGE,
        currentPage
    }
}

export const headerOptionsStateChange = (optionsOpen) => {
    return {
        type: HeaderNavConstants.HEADER_OPTIONS_STATE_CHANGE,
        optionsOpen
    }
}

export default navStateChange;//{navStateChange, headerOptionsStateChange}