'use strict';

import React, {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    Component,
    ListView
} from 'react-native';

import StateIndicator from './NavStateIndicator';

class HeaderNav extends Component {
    constructor(props) {
        super(props);
        //sets up all the nav state; true defines current state user is on
        let navStates = [
            'explore',
            'discover',
            'history'
        ];
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            navStates: navStates, 
            dataSource: ds.cloneWithRows(['Explore', 'History'])
        };
    }

    openOptionsNav() {
        console.log('i am opening option');
        //this.state.openOptions
        /*if (this.state.openOptions) {
            this.setState({openOptions: false})
        } else {
            this.setState({openOptions: true})
        }*/
        if (this.props.optionsOpen) {
            this.props.headerOptionsStateActions(false);
        } else {
            this.props.headerOptionsStateActions(true);
        }
        //this.setState({openOptions: !this.state.openOptions});
    }

    takePhoto() {
        //TODO, look at logic from Stickers-camera.js
        console.log('take photo');
    }

    openPage(data) {
        //this.props.navStateActions(data);
        //console.log(this.props.navStateActions, 'what is this???');
        //console.log(data, 'open page');
    }

    buildNavStateIndicators() {
        let stateIndicators = [];
        {this.state.navStates.map((key) => {
            let status = (key === this.props.currentPage);
            stateIndicators.push(
                <StateIndicator
                    key={key}
                    status={status}
                />);
        })}
        return stateIndicators;
    }

    closeOptions() {
        console.log('close ok');
    }

    render() {
        /*let renderState = (
            <View style={styles.header}>
                <View style={styles.headerButtonHolder}>
                    <TouchableOpacity
                        onPress={() => this.openOptionsNav()}
                    >
                        <Image
                            source={require('./assets/optionsNav.png')}
                            style={{width:35, height:35}}
                        />
                    </TouchableOpacity>
                </View>
                <View style={styles.headerIndicator}>
                    <Text style={styles.headerIndicatorText}>
                    {'Explore'}
                    </Text>
                    <View style={styles.indicatorCircleHolder}>
                        {this.buildNavStateIndicators()}

                    </View>
                </View>
                <View style={styles.headerButtonHolder}>
                    <TouchableOpacity
                        onPress={() => this.takePhoto()}
                    >
                        <Image
                            source={require('./assets/addPhoto.png')}
                            style={{width:35, height:35}}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        );*/

        let optionState;
        
        return (
            <View style={styles.header}>
                <View style={styles.headerButtonHolder}>
                    <TouchableOpacity
                        onPress={() => this.openOptionsNav()}
                    >
                        <Image
                            source={require('./assets/optionsNav.png')}
                            style={{width:35, height:35}}
                        />
                    </TouchableOpacity>
                </View>
                <View style={styles.headerIndicator}>
                    <Text style={styles.headerIndicatorText}>
                    {this.props.currentPage}
                    </Text>
                    <View style={styles.indicatorCircleHolder}>
                        {this.buildNavStateIndicators()}

                    </View>
                </View>
                <View style={styles.headerButtonHolder}>
                    <TouchableOpacity
                        onPress={() => this.takePhoto()}
                    >
                        <Image
                            source={require('./assets/addPhoto.png')}
                            style={{width:35, height:35}}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    header:{
        height:65,
        flexDirection: 'row',
        backgroundColor:'white'
    },
    headerButtonHolder:{
        height:65,
        width:65,
        justifyContent:'center',
        alignItems:'center'
    },
    headerIndicator:{
        flex:2,
        justifyContent:'space-around'
    },
    headerIndicatorText:{
        textAlign:'center',
        paddingTop:20,
        fontFamily: 'Arial-BoldMT',
        fontSize: 18
    },
    headerRight:{
        height:65,
        width:65,
        backgroundColor: 'red'
    },
    indicatorCircleHolder:{
        flexDirection: 'row',
        alignSelf: 'center',
    }
});

module.exports = HeaderNav;
