import HeaderNavConstants from '../constants/HeaderNavConstants';

const headerNavStateChange = (state = {currentPage:'explore', optionsOpen: false}, action) => {
    switch (action.type) {
    case HeaderNavConstants.HEADERNAV_STATE_CHANGE:
        return { ...state, currentPage: action.currentPage, optionsOpen: false }
    case HeaderNavConstants.HEADER_OPTIONS_STATE_CHANGE:
        return { ...state, optionsOpen: action.optionsOpen}
    default:
        return state
    }
}

export default headerNavStateChange;
