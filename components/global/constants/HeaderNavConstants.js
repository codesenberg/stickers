
var keyMirror = require('keymirror');

module.exports = keyMirror({
    HEADERNAV_STATE_CHANGE: null,
    HEADER_OPTIONS_STATE_CHANGE: null
});
