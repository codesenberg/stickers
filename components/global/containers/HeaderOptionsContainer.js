import { connect } from 'react-redux';
import  * as headerNavActions  from '../actions/headerNavAction';

import {bindActionCreators} from 'redux';
import HeaderOptions from '../HeaderOptions';

/*
import { connect } from 'react-redux';
import HeaderNav from '../HeaderNav';
import * as headerNavActions from '../actions/headerNavAction';
import headerOptionsStateChange from '../actions/headerNavAction';
import {bindActionCreators} from 'redux';*/


const mapStateToProps = (state) => {
    return {
        currentPage: state.headerNavStateChange.currentPage
    }
}

const mapDispatchToProps = (dispatch) => ({
    navStateActions: bindActionCreators(headerNavActions.navStateChange, dispatch),
    headerOptionsStateActions: bindActionCreators(headerNavActions.headerOptionsStateChange, dispatch)
});

const HeaderOptionsContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(HeaderOptions);

export default HeaderOptionsContainer;
