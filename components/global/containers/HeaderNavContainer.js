import { connect } from 'react-redux';
import HeaderNav from '../HeaderNav';
import * as headerNavActions from '../actions/headerNavAction';
//import headerOptionsStateChange from '../actions/headerNavAction';
import {bindActionCreators} from 'redux';

const mapStateToProps = (state) => {
    return {
        currentPage: state.headerNavStateChange.currentPage,
        optionsOpen: state.headerNavStateChange.optionsOpen
    }
}

const mapDispatchToProps = (dispatch) => ({
    navStateActions: bindActionCreators(headerNavActions.navStateChange, dispatch),
    headerOptionsStateActions: bindActionCreators(headerNavActions.headerOptionsStateChange, dispatch)
});

const HeaderNavContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(HeaderNav);

export default HeaderNavContainer;
