'use strict';

import React, {
    Text,
    View,
    Component,
    ListView
} from 'react-native';


class HeaderOptions extends Component {
    constructor(props) {
        super(props);
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            dataSource: ds.cloneWithRows(['Explore', 'History'])
        };
    }

    openPage(data) {
        console.log(data, 'what page is opened');
        //console.log(this.props.navStateActions, 'this.props.navStateActions')
        this.props.navStateActions(data);
    }

    render() {
        return (
            <View>
                <ListView
                    dataSource={this.state.dataSource}
                    renderRow={(data) =>
                        <View>
                            <Text onPress={() => this.openPage(data.toLowerCase())}>
                                Click here to navigate to: {data}
                            </Text>
                        </View>
                    }
                />
            </View>);
    }
}

module.exports = HeaderOptions;
