'use strict';

import React, {
    StyleSheet,
    View,
    Component
} from 'react-native';

class StateIndicator extends Component {
    indicatorStyle() {
        let myStyle;
        if (this.props.status) {
            myStyle = [styles.indicatorCircle, styles.indicatorCurrent];
        } else {
            myStyle = [styles.indicatorCircle];
        }

        return myStyle;
    }

    render() {
        return (
            <View style={this.indicatorStyle()}/>
        );
    }
}
const styles = StyleSheet.create({
    indicatorCircle: {
        width: 7,
        height: 7,
        borderRadius: 100 / 2,
        backgroundColor: '#d6d6d6',
    },
    indicatorCurrent: {
        backgroundColor: 'black'
    }
});

module.exports = StateIndicator;
