const HEADERS_JSON = {
    'Accept': 'text/json',
    'Content-Type': 'application/json',
};

const handleErrors = (response) => {
    if (!response.ok) {
        throw Error(response._bodyText);
    }
    return response;
}

class api {
    static post(url, params = {}) {
        return fetch(url, {
            method: 'POST',
            headers: HEADERS_JSON,
            body: JSON.stringify(params)
        })
        .then(handleErrors);
    }
}

module.exports = api;
