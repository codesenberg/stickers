'use strict';

import React, {
    Component,
    StyleSheet,
    Text,
    View,
    Image,
    ScrollView,
    TextInput,
    TouchableHighlight
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import ExploreContainer from './components/explore/containers/ExploreContainer';
import * as exploreActions from './components/explore/actions/exploreAction';
import HeaderNavContainer from './components/global/containers/HeaderNavContainer';
import HeaderOptionsContainer from './components/global/containers/HeaderOptionsContainer';
import LoginContainer from './components/login/containers/LoginContainer';
import { connect } from 'react-redux'
import navStateChange from './components/global/actions/headerNavAction';
import {bindActionCreators} from 'redux';

//mock data for explores
let sampleText= 'hello world, hello world, hello, hello world, hello world, hello, hello world, hello world, hello';
let mockExplores = {
    explore1: {
        userIcon:{uri: 'http://localhost:8888/php/users/user2/profile-image/main.png'},
        imageIsLiked:false,
        image: {uri: 'http://localhost:8888/php/users/user2/posted-images/post1.jpg'},
        messageIsLiked:true,
        message:sampleText
    },
    explore2: {
        userIcon:require('./assets/mock/global/main-user-icon.png'),
        imageIsLiked:false,
        image:require('./assets/mock/explore/image1.png')
    },
    explore3: {
        userIcon:require('./assets/mock/global/main-user-icon.png'),
        messageIsLiked:false,
        message:sampleText
    }
};

let mockHistory = mockExplores;

class Stickers extends Component {
    constructor(props){
        super(props);
        this.state = {
            mockHistory: mockHistory
        }
        //load explore data
        props.fetchExplores({uid:'user2'});
    }

    updateLikeStatus(key, postId, status) {
        //props.exploreActions({uid:'user2', status, postId, });
        //TODO THIS WILL BE WRAPPED INSIDE A DB callback
        /*this.setState(function(previousState, newState) {
            //TODO AFTER WRAPPING INSIDE DB CALL SEE IF I NEED TO USE COPY ON PREVIOUS STATE
            mockExplores[key][contentId] = status;
            return {mockExplores:mockExplores};
        });*/
    }

    buildExplores() {
        let explores = [];

        if (this.props.exploresLoaded) {
            {Object.keys(this.props.posts).map((key) => {
                let currentImage = (this.props.posts[key].image) ? {uri:this.props.posts[key].image} : false;
                let imageIsLiked = (this.props.likedPosts[key] !== undefined && this.props.likedPosts[key].hasOwnProperty('image')) ? this.props.likedPosts[key].image : false;
                let messageIsLiked = (this.props.likedPosts[key] !== undefined && this.props.likedPosts[key].hasOwnProperty('text')) ? this.props.likedPosts[key].text : false;
                explores.push(
                    <ExploreContainer
                        id={key}
                        ownerId={this.props.posts[key].ownerId}
                        image={currentImage}
                        imageIsLiked={imageIsLiked}
                        key={key}
                        message={this.props.posts[key].text}
                        messageIsLiked={messageIsLiked}
                        type={'explore'}
                        updateLikeStatus={(key, contentId, status) => this.updateLikeStatus(key, contentId, status)}
                        userIcon={{uri:this.props.posts[key].userIcon}}
                    />);

            })}

            if (explores.length === 0) {
                explores = <Text>{'No Explores'}</Text>
            }
        } else {
            explores = <Text>{'Loading'}</Text>
        }

        return explores;
    }

    buildHistory() {
        let history = [];
        {Object.keys(this.state.mockHistory).map((key) => {
            history.push(
                <ExploreContainer
                    id={key}
                    image={mockHistory[key].image}
                    imageIsLiked={mockHistory[key].imageIsLiked}
                    key={key}
                    message={mockHistory[key].message}
                    type={'history'}
                    userIcon={mockHistory[key].userIcon}
                />);

        })}
        return history;
    }

    render() {
        let renderState;

        //temporaliy used to handle states until proper routing is setup
        if (this.props.loginStatus.status !== 'success') {
            //LOGIN
            renderState = (
                <View style={styles.container}>
                    <ScrollView
                        automaticallyAdjustContentInsets={false}
                        horizontal={false}
                    >
                        <LoginContainer/>
                    </ScrollView>
                </View>
            );
        } else if (this.props.navState.optionsOpen) {
            //OPEN HEADER OPTIONS
            renderState =  (
                <View style={styles.container}>
                    <HeaderNavContainer/>
                        <HeaderOptionsContainer/>
                </View>);
        } else if (this.props.navState.currentPage === 'history') {
            //HISTORY VIEW
            renderState = (
                <View style={styles.container}>
                    <HeaderNavContainer/>
                    <ScrollView
                        automaticallyAdjustContentInsets={false}
                        horizontal={false}
                    >
                        {this.buildHistory()}
                    </ScrollView>
                </View>
            );
        } else if (this.props.navState.currentPage === 'explore') {
            //EXPLORE VIEW
            renderState = (
                <View style={styles.container}>
                    <HeaderNavContainer/>
                    <ScrollView
                        automaticallyAdjustContentInsets={false}
                        horizontal={false}
                    >
                        {this.buildExplores()}
                    </ScrollView>
                </View>
            );
        }
        
        return renderState;
    }
}

Stickers.defaultProps = {
    posts: {},
    exploresLoaded: false
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
        paddingTop:20,
        flexDirection:'column'
    }
});

const mapStateToProps = (state) => {
    return {
        posts: state.exploreStore.posts,
        exploresLoaded: state.exploreStore.exploresLoaded,
        likedPosts: state.exploreStore.likedPosts,
        loginStatus: state.loginStore,
        navState: state.headerNavStateChange
    }
};

const mapDispatchToProps = (dispatch) => ({
    navStateActions: bindActionCreators(navStateChange, dispatch),
    fetchExplores: bindActionCreators(exploreActions.fetchExplores, dispatch)
});

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Stickers);
