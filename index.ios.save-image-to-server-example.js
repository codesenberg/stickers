//this contains logic on uploading image to server plus searching camera roll if needs be

const React = require('react-native');

const {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    ScrollView,
    Image,
    CameraRoll,
    TouchableHighlight,
    NativeModules,
} = React;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    imageGrid: {
        flex: 1,
        flexDirection: 'row',
        flexWrap: 'wrap',
        justifyContent: 'center'
    },
    image: {
        width: 100,
        height: 100,
        margin: 10,
    }
});

//fileKey, // (default="file") the name of the field in the POST form data under which to store the file
/*data: {
    // whatever properties you wish to send in the request
    // along with the uploaded file
}*/
var myHeaders = new Headers();
myHeaders.append('Content-Type', 'image/jpeg');
var obj = {
    uri:'assets-library://asset/asset.JPG?id=07F3CC89-26F7-43A4-9A95-D988D7D8B024&ext=JPG', // either an 'assets-library' url (for files from photo library) or an image dataURL
    uploadUrl:'http://localhost:8888/php/api/save-image.php',
    fileName:'omar2.jpg',
    mimeType:'image/jpeg',
    headers:myHeaders
};

const reactImageProject = React.createClass({
    getInitialState() {

        //console.log(NativeModules.FileTransfer,'NativeModules.FileTransfer.upload');
        NativeModules.FileTransfer.upload(obj, (err, res) => {
            console.log('*****');
            console.log(err,res);
            console.log('*****');
            // handle response
            // it is an object with 'status' and 'data' properties
            // if the file path protocol is not supported the status will be 0
            // and the request won't be made at all
        });
        return {
            images: [],
            selected: '',
        };
    },

    componentDidMount() {
        const fetchParams = {
            first: 25,
        };
        //console.log(CameraRoll)
        CameraRoll.getPhotos(fetchParams).
        then(this.storeImages).
        catch(this.logImageError);
    },

    storeImages(data) {
        //console.log(data,'this is my data');
        const assets = data.edges;
        const images = assets.map((asset) => asset.node.image);
        this.setState({
            images: images,
        });
    },

    logImageError(err) {
        console.log(err);
    },

    selectImage(uri) {
        NativeModules.ReadImageData.readImage(uri, (image) => {
            this.setState({
                selected: image,
            });
            console.log(image);
        });
    },

    render() {
        return (
            <ScrollView style={styles.container}>
                <View style={styles.imageGrid}>
                { this.state.images.map((image) => {
                    return (
                        <TouchableHighlight onPress={this.selectImage.bind(null, image.uri)}>
                        <Image style={styles.image} source={{ uri: image.uri }} />
                        </TouchableHighlight>
                    );
                    })
                }
                </View>
            </ScrollView>
        );
    }
});

AppRegistry.registerComponent('stickers', () => reactImageProject);
