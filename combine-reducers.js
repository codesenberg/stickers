import { combineReducers } from 'redux'
import headerNavStateChange from './components/global/reducers/headerNav';
import exploreStore from './components/explore/reducers/exploreStore';
import loginStore from './components/login/reducers/loginStore';

const rootReducer = combineReducers({headerNavStateChange, exploreStore, loginStore});

export default rootReducer;
