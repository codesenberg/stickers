'use strict'
/* eslint react/no-multi-comp: 0 */

import React, {Component} from 'react-native';
import { applyMiddleware, createStore } from 'redux';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
//temporary until i create a proper system for centralizing reducers
import rootReducer from './combine-reducers';
import Stickers from './Stickers';

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
const store = createStoreWithMiddleware(rootReducer);

class reduxWrapper extends Component {
    render() {
        return (
            <Provider store={store}>
                <Stickers />
            </Provider>
        )
    }
}

export default reduxWrapper;
