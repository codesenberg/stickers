'use strict';

//TODO integrate in main Stickers application
import React, {
    Component,
    StyleSheet,
    Text,
    View,
    Image,
    TouchableHighlight
} from 'react-native';
import Camera from 'react-native-camera';
import { connect } from 'react-redux'
import {bindActionCreators} from 'redux';

class Stickers extends Component {
    constructor(props){
        super(props);
        this.state = {
            cameraType: Camera.constants.Type.back
        };
    }

    //simulates taking picture with front camera or back camera
    _switchCamera() {
        var state = this.state;
        state.cameraType = state.cameraType === Camera.constants.Type.back ? Camera.constants.Type.front : Camera.constants.Type.back;
        this.setState(state);
    }

    _takePicture() {
        this.refs.cam.capture((err, data)=> {
            this.state.myImage = {uri:data};
            this.setState(this.state);
            console.log(err, data);
        });
    }

    render() {
        return (
            <Camera
                ref="cam"
                style={styles.container}
                type={this.state.cameraType}>
                <View style={styles.buttonBar}>
                    <Image
                        source={this.state.myImage}
                        style={{height:100,width:100}}
                    />
                    <TouchableHighlight style={styles.button} onPress={()=>this._switchCamera()}>
                        <Text style={styles.buttonText}>Flip</Text>
                    </TouchableHighlight>
                    <TouchableHighlight style={styles.button} onPress={()=>this._takePicture()}>
                        <Text style={styles.buttonText}>Take</Text>
                    </TouchableHighlight>
                </View>
            </Camera>
        );
    }
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "transparent",
    },
    buttonBar: {
        flexDirection: "row",
        position: "absolute",
        bottom: 25,
        right: 0,
        left: 0,
        justifyContent: "center"
    },
    button: {
        padding: 10,
        borderWidth: 1,
        borderColor: "#FFFFFF",
        margin: 5
    },
    buttonText: {
        color: "white"
    }
});

export default connect(
    '',
    ''
)(Stickers);
